class CreateCups < ActiveRecord::Migration
  def change
    create_table :cups do |t|
      t.string :name
      t.float :price
      t.string :image

      t.timestamps
    end
  end
end