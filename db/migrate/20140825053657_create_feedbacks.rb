class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.string :email
      t.text :message
      t.string :phone
      t.string :subject

      t.timestamps
    end
  end
end
