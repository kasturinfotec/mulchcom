class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.text :about_mulch
      t.text :why_mulch
      t.text :How_it_works
      t.text :terms_and_conditions

      t.timestamps
    end
  end
end
