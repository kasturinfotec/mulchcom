class CreateDonationCups < ActiveRecord::Migration
  def change
    create_table :donation_cups do |t|
      t.references :donation
      t.references :cup

      t.timestamps
    end
  end
end
