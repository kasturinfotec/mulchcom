require 'test_helper'

class PaymentControllerTest < ActionController::TestCase
  test "should get checkout" do
    get :checkout
    assert_response :success
  end

  test "should get success" do
    get :success
    assert_response :success
  end

  test "should get cancle" do
    get :cancle
    assert_response :success
  end

end
