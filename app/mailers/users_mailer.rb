class UsersMailer < ActionMailer::Base

  def welcome_message user
	@user = user
	mail(:to => user.email, :subject => "Welcome to Mulch!", :from => "mulchcom@gmail.com")
  end

  def payment_success res, user
  	@res = res
  	@user = user
  	mail(:to => "unitedsponsorsofamerica@gmail.com", :subject => "Paypal payment successfully done!", :from => "mulchcom@gmail.com")
  end

  def pay_in_person res, user
  	@res = res
  	@user = user
    mail(:to => "unitedsponsorsofamerica@gmail.com", :subject => "Pay In Person!", :from => "mulchcom@gmail.com")
  end

  def child_referred user, child
    @user = user
    @child = child
    mail(:to => "unitedsponsorsofamerica@gmail.com", :subject => "You are reffered by sponsor #{@user.full_name}!", :from => "mulchcom@gmail.com")
  end

  def child_enrolled user, child
    @user = user
    @child = child
    mail(:to => "unitedsponsorsofamerica@gmail.com", :subject => "Enrolled New Child!", :from => "mulchcom@gmail.com")
  end

  def invite_referral email, inviter ,child
    @inviter = inviter
    @child = child
    mail(:to => email, :subject => "Mulch Invitation", :from => "mulchcom@gmail.com")
  end

  def feedback params
    @params = params
  	mail(:to => "unitedsponsorsofamerica@gmail.com", :subject => "Mulch Feedback", :from => "mulchcom@gmail.com")
  end


end
