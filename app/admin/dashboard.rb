ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

    # section "Recent Donation" do
    #     table_for donation.order("released_at desc").limit(5) do
    #         column :name
    #         column :user_id
    #         column :payment_date
    #       end  
            

  content :total_contribution => proc{ I18n.t("active_admin.dashboard") } do
    div :class => "blank_slate_container", :id => "dashboard_default_message" do
      span :class => "blank_slate" do
        h3 "Welcome to Mulch Admin."
      end
    end

    # Here is an example of a simple dashboard with columns and panels.

    columns do
      column do
        panel "Recent Donations Done" do
          ul do
            Donation.all.map do |donation|
              li link_to(donation.total_contribution, admin_donation_path(donation))
            end
          end
        end
      end

      column do
        panel "Recent Contributions" do
          ul do
            Contribution.all.map do |contribution|
              li link_to(contribution.total_contribution, admin_contribution_path(contribution))
            end
          end
        end
      end
      
     

    end
  end 

end
