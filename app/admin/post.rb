ActiveAdmin.register Post do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :about_mulch,:why_mulch,:How_it_works,:terms_and_conditions, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  form do |f|
    f.inputs "Details" do
      f.input :about_mulch, as: :ckeditor, label: true
      f.input :why_mulch, as: :ckeditor, label: true
      f.input :How_it_works, as: :ckeditor, label: true
      f.input :terms_and_conditions, as: :ckeditor, label: true

    f.actions
    end
  end

end
