ActiveAdmin.register Feedback do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
   permit_params  :email,:subject,:phone, :message
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  index do
    selectable_column
    id_column
    column :email
    column :subject
    column :message
    column :phone
    column :created_at 
    actions
  end
  
  filter :email
  filter :created_at
  #

end
