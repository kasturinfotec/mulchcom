ActiveAdmin.register User do

scope :all, :default => true

   # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
   permit_params :first_nm, :last_nm, :email, :dob, :appt_no, :st_no, :city, :country, :phone, :mobile, :avatar, :role, :ssn, :gender, guardians_attributes:[:id, :appt_no, :st_no, :city, :first_nm, :last_nm, :dob, :address, :phone] rescue   nil
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
