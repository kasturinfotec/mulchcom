class UsersController < ApplicationController
	before_filter :authenticate_user!, except: [:member_associate]
	protect_from_forgery with: :null_session
   before_action :set_User, only: [:show, :edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @users = User.all

  end

  # GET /users/new
  def new
    @users = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # User /users
  # User /users.json
  def create
    @users = User.new(User_params)

    respond_to do |format|
      if @users.save
        format.html { redirect_to @users, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @users }
      else
        format.html { render :new }
        format.json { render json: @users.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @users.update(User_params)
        format.html { redirect_to @users, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @users }
      else
        format.html { render :edit }
        format.json { render json: @users.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @users.destroy
    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_User
       @users = User.find(params[:User_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def User_params
      params.require(:User).permit(:about_mulch,:why_mulch,:How_it_works,:terms_and_conditions)
    end



	def update_profile
		@user = User.find(current_user.id)
	end

	def save_profile
		begin
			@user = User.find(current_user.id)
			unless params[:user][:guardians_attributes].blank?
				params[:user][:guardians_attributes].each_with_index do |guardian, index|
					Guardian.find(guardian[1][:id]).update_attributes(first_nm: guardian[1][:first_nm], last_nm: guardian[1][:last_nm], dob: guardian[1][:dob], appt_no: guardian[1][:appt_no], st_no: guardian[1][:st_no], city: guardian[1][:city], phone: guardian[1][:phone]) if index == 0
					Guardian.find(guardian[1][:id]).update_attributes(first_nm: guardian[1][:first_nm], last_nm: guardian[1][:last_nm], dob: guardian[1][:dob], address: guardian[1][:address], phone: guardian[1][:phone]) if index == 1
				end
			end
			params[:user][:dob] = params[:user][:dob]
			if @user.update_attributes(as_params(params))
				@flash = { status: :success, message: 'Profile has been successfully updated' }
				flash.now[:success] = 'Profile has been successfully updated'
				sign_in @user, :bypass => true
			else
				@flash = { status: :error, message: @user.errors.full_messages.join(', ') }
				flash.now[:error] = @user.errors.full_messages.join(', ')
			end
		rescue Exception => e
			@flash = { status: :error, message: e.message }
			flash.now[:error] = e.message
		end
		render "/users/update_profile.html.erb" unless request.xhr?
	end

	def save_child
		begin
			@child = User.new(as_params_child(params))
			@child.dob = params[:user][:dob]
			@child.password = 12345678
			@child.role = "children"
			if @child.save
				unless params[:user][:guardians_attributes].blank?
					params[:user][:guardians_attributes].each_with_index do |guardian, index|
						Guardian.create(first_nm: guardian[1][:first_nm], last_nm: guardian[1][:last_nm], dob: guardian[1][:dob], appt_no: guardian[1][:appt_no], st_no: guardian[1][:st_no], city: guardian[1][:city], phone: guardian[1][:phone], child_id: @child.id) if index == 0
						Guardian.create(first_nm: guardian[1][:first_nm], last_nm: guardian[1][:last_nm], dob: guardian[1][:dob], address: guardian[1][:address], phone: guardian[1][:phone], child_id: @child.id) if index == 1
					end
				end
				@flash = { status: :success, message: 'Child has been successfully enrolled!' }
				UsersMailer.child_referred(current_user, @child)
				UsersMailer.child_enrolled(current_user, @child)
				flash[:success] = 'Child has been successfully enrolled!'
				@redirect_to_path = registered_children_path
			else
				@flash = { status: :error, message: @child.errors.full_messages.join(', ') }
				flash.now[:error] = @child.errors.full_messages.join(', ')
				# redirect_to pledge_now_path
			end
		rescue Exception => e
			@flash = { status: :error, message: e.message }
			flash[:error] = e.message
			# redirect_to = pledge_now_path
		end
	end

	def buddy_referrals
		referrals = User.where(:referral_id => current_user.id)
		@referrals = referrals.map { |children| {:id => children.id, :first_nm => children.first_nm, :last_nm => children.last_nm, :ref_id => children.ref_id, profile: children.avatar.url, email: children.email} } unless referrals.empty?
		@childrens = current_user.children
	end

	def invite_referral
		begin
			puts"*****************"
			temp = params
			emails = params[:email].split(",")
			child = params[:child]
			UsersMailer.invite_referral(emails, current_user, child).deliver
			message = ["Invite mails have been successfully sent to (#{emails.join(',')})."]
			
			@flash = { status: :success, message: message.join(" ") }
		rescue Exception => e
			@flash = { success: false, message: e.message }
		end
	end

	def member_associate
		if user_signed_in?
			@user = User.find(current_user.id)
			render '/users/member_associate.html.erb'
		else
			render '/misc/become_member.html.erb'
		end
	end

	def change_password
		@user = User.find(current_user.id)
		if request.User?
			if  @user.valid_password?(params[:user][:current_password])
				if current_user.update_with_password(as_params_pwd(params))
					sign_in(current_user, :bypass => true)
					flash.now[:success] = "Password has been successfully changed!"
				else
					flash.now[:error] = current_user.errors.full_messages.join(", ")
				end
			else
				flash.now[:error] = "Current password didn't match!"
			end
		end
	end

	def registered_children
		@children = current_user.children 
	end

	private
		def needs_password?(user, params)
			user.email != params[:user][:email] ||
			params[:user][:password].present? ||
			params[:user][:password_confirmation].present?
		end
	protected
		def as_params params
			params.require(:user).permit(:id, :dob, :first_nm, :last_nm, :appt_no, :st_no, :city, :phone, :mobile, :avatar, :role, :ssn, :gender, guardians_attributes:[:id, :appt_no, :st_no, :city, :first_nm, :last_nm, :dob, :address, :phone])
		end

		def as_params_child params
			params.require(:user).permit(:id, :parent_id, :first_nm, :last_nm, :email, :dob, :appt_no, :st_no, :city, :phone, :mobile, :avatar, :role, :ssn, :gender, guardians_attributes:[:id, :appt_no, :st_no, :city, :first_nm, :last_nm, :dob, :address, :phone])
		end

		def as_params_pwd params
			params.require(:user).permit(:current_password, :password, :password_confirmation)
		end
		# def user_params params
		# params.require(:user).permit(:first_nm,:first_nm,:ref_id, :dob ,:gender)
		# end
end