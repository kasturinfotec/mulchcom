class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }
  before_filter :configure_permitted_parameters, if: :devise_controller?

   def ensure_signup_complete
    # Ensure we don't go into an infinite loop
    return if action_name == 'finish_signup'

    # Redirect to the 'finish_signup' page if the user
    # email hasn't been verified yet
    if current_user && !current_user.email_verified?
      redirect_to finish_signup_path(current_user)
    end
  end
  
  protected
    
    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:email, :password) }
      devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:first_nm, :last_nm, :email, :password, :password_confirmation, :iaccept, :role, :ref_id, :referral_id, :ssn, :username, :role) }
      devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:username, :email, :password, :password_confirmation, :current_password, :role) }
    end

    def after_sign_in_path_for(resource)
      flash.delete(:notice)
      if session[:ref_id].nil?
        root_path
      else
        "/pledge-now?ref_id=#{session[:ref_id]}"
      end
    end

    # def after_update_path_for(resource)
    #   profile_update_path(resource)
    # end

  private
    def under_construction
      render template: '/misc/under-const.html'
    end
    
    def strptime date
      d = date.split('/')
      # return Date.strptime("#{d[1]}-#{d[0]}-#{d[2]}", '%d-%m-%Y')
      return "#{d[1]}-#{d[0]}-#{d[2]}"
    end

end