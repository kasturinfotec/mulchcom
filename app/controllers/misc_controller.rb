class MiscController < ApplicationController

	def pledge_now
		if !user_signed_in?
    		users = User.all_children.order("RANDOM()").limit(12)#.page(params[:page]).per(5)
    		@children = users.map { |children| {:id => children.id, :first_nm => children.first_nm, :last_nm => children.last_nm, :ref_id => children.ref_id, profile: "#{children.avatar.url rescue  '/assets/photo_not_found.png' }", email: children.email} }
    		render "/misc/children.html.erb"
    	else
			@user = User.new
			@user.guardians.new
			@user.guardians.new
			@cups = Cup.all
			@child = User.find_by_ref_id(params[:ref_id]) unless params[:ref_id].nil?
			@frequencies = Frequency.all
    	end
	end

	def why
		@post = Post.last
	end

	def feedback
		if request.post?
			@feedback = Feedback.new(params.require(:feedback).permit(:email, :message, :phone, :subject))
			if @feedback.save
				UsersMailer.feedback(params[:feedback]).deliver
				flash.now[:success] = "Your feedback has been sent successfully!"
				@feedback = Feedback.new
			else
				flash.now[:error] = @feedback.errors.full_messages.join(", ")
			end
		else
			@feedback = Feedback.new
		end
	end
def userscr
	
end
end
