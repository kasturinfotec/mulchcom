module MiscHelper

	# Generate dummy transaction id for session
	def dummy_transaction_id user_id, child_id
		"MUL#{user_id}#{child_id}#{Time.now.to_i}"
	end
end
