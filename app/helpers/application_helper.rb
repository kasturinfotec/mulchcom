module ApplicationHelper
	def flash_class(level)
		case level
		when :notice then "info_msg"
		when :success then "succ_msg"
		when :error then "err_msg"
 		when :alert then "info_msg"
		end
	end

	def register_as_child?
		controller_name == 'misc' && action_name == 'pledge_now' && !@is_new_user
	end

	def become_member_associate?
		controller_name == 'users' && action_name == 'member_associate'
	end

	def format_ssn ssn
		"xxx-xx-#{ssn[5..8]}"
	end
end
