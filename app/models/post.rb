# == Schema Information
#
# Table name: posts
#
#  id                   :integer          not null, primary key
#  about_mulch          :text
#  why_mulch            :text
#  How_it_works         :text
#  terms_and_conditions :text
#  created_at           :datetime
#  updated_at           :datetime
#

class Post < ActiveRecord::Base
end
